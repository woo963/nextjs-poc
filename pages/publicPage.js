import {signIn, useSession} from "next-auth/client";
import Link from "next/link";
import styles from "../styles/Home.module.css"
import Head from "next/head";

export default function PublicPage() {
  const [session, loading] = useSession();
  return (
    <div className={styles.container}>
      <Head>
        <title>Public page</title>
      </Head>
    <div className={styles.main}>
      {!session && (
        <>
          <h1> To view the data, you need to log in</h1>
          <button>
            <Link href={`/`} replace>Back</Link>{" "}
          </button>
        </>)}
    </div>
    </div>
  );
}