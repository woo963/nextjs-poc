import {getSession} from "next-auth/client";

export default async function (req, res) {
    const user = await getSession({req});
    if (user) {
        try {
            const response = await fetch(
              `https://api.openweathermap.org/data/2.5/onecall?lat=53.195873&lon=50.100193&units=metric&exclude=hourly,alerts,minutely,current&appid=373651eef091af2d1670e866f24aa876`
            )
            const weather = await response.json()
            return res.json(weather)
        } catch (e) {
            console.log(e.message())
        }
    } else {
        res.json({error: "Not logged in"});
    }
};