import {signOut, useSession} from "next-auth/client";
import WeatherCard from "../components/WeatherCard";
import Link from "next/link";
import style from "../styles/weatherForecast.module.css"
import Head from "next/head";

export default function WeatherForecast({ data }) {
  const [session, loading] = useSession();
  const nowDate = new Date();
  const months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
  const weatherForecast = []
  data.daily.map((dayWeather, index) =>
    weatherForecast.push({
      temp: Math.floor(dayWeather.temp.day),
      icon: dayWeather.weather[0].icon,
      date: nowDate.getDate() + index + ' ' + months[nowDate.getMonth()] + ' ' + nowDate.getFullYear()
    }))
  return (
    <div className={style.container}>
      <Head>
        <title>Private page</title>
      </Head>
      <h1>Weather forecast for week</h1>
      <div className={style.dailyForecastBlock}>
        <div className={style.dailyForecastBlock__cards}>
          {weatherForecast.map((dayWeather, index) =>
            <WeatherCard
              key={index}
              temp={dayWeather.temp}
              icon={dayWeather.icon}
              date={dayWeather.date}
            />
          )}
        </div>
      </div>
      <button>
        <Link href={`/`}><a>Back</a></Link>{" "}
      </button>
      <button onClick={() => signOut({callbackUrl: "/"})}>Sign out</button>
    </div>
  );
}
export async function getServerSideProps(context) {
  const hostname = "http://localhost:3000";
  const options = { headers: { cookie: context.req.headers.cookie } };
  const res = await fetch(`${hostname}/api/forecast/forecast`, options);
  const json = await res.json();
  if (json.error) {
    return {
      redirect: {
        destination: "/publicPage",
        permanent: false,
      },
    };
  }
  return {
    props: {
      data: json,
    },
  };
}