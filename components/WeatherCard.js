import style from './WeatherCard.module.css'

export default function WeatherCard (props) {
  return (
    <div className={style.dayWeatherCard}>
      <span className={style.dayWeatherCard__date}>{props.date}</span>
      <img src= {`https://openweathermap.org/img/wn/${props.icon}@2x.png`} alt="error" className={style.dayWeatherCard__icon}/>
      <h2 className={style.dayWeatherCard__temp}>+{props.temp}°</h2>
    </div>
  )
}